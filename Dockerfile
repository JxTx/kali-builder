FROM kalilinux/kali-rolling
RUN apt-get update && \
    apt-get install -y curl git live-build cdebootstrap fdisk
RUN git clone https://gitlab.com/kalilinux/build-scripts/live-build-config.git

WORKDIR /live-build-config/
ENTRYPOINT ["./build.sh"]
