# Kali Builder 

Docker container for [this](https://www.kali.org/docs/development/live-build-a-custom-kali-iso/). 

## Build

The pipeline does this for you ;) 

## Running

The container requires the `--privileged` option and `/proc` mounted. Your also going to want to mount a direcotry to `/live-build-config/images/` within the container to have the `.iso` once built.

Its been tested with this command.

```bash
docker run --rm --privileged -v /proc:/proc -v $(pwd)/images/:/live-build-config/images/ registry.gitlab.com/jxtx/kali-builder --variant i3wm --verbose
```

And after sometime.. 

```bash
***
GENERATED KALI IMAGE: ./images/kali-linux-rolling-live-i3wm-amd64.iso
***
```

### Pipeline runner

Because of the required flags to docker, its not possible to have the pipeline build the `.iso`. It may be possible to run this within a [registered runner](https://docs.gitlab.com/runner/register/), but ensure that the `privileged` option is set to **True**.
